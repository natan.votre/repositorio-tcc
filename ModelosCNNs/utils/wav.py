import struct
import io

import warnings
import time

WAVE_FORMAT_PCM = 0x0001
WAVE_FORMAT_IEEE_FLOAT = 0x0003
WAVE_FORMAT_EXTENSIBLE = 0xfffe
KNOWN_WAVE_FORMATS = (WAVE_FORMAT_PCM, WAVE_FORMAT_IEEE_FLOAT)



def _skip_unknown_chunk(fid, is_big_endian):
    if is_big_endian:
        fmt = '>I'
    else:
        fmt = '<I'

    data = fid.read(4)
    # call unpack() and seek() only if we have really read data from file
    # otherwise empty read at the end of the file would trigger
    # unnecessary exception at unpack() call
    # in case data equals somehow to 0, there is no need for seek() anyway
    if data:
        size = struct.unpack(fmt, data)[0]
        fid.seek(size, 1)


def _read_riff_chunk(fid):
    str1 = fid.read(4)  # File signature
    # print(str1)
    if str1 == b'RIFF':
        is_big_endian = False
        fmt = '<I'
    elif str1 == b'RIFX':
        is_big_endian = True
        fmt = '>I'
    else:
        # There are also .wav files with "FFIR" or "XFIR" signatures?
        raise ValueError("File format {}... not "
                         "understood.".format(repr(str1)))

    # Size of entire file
    file_size = struct.unpack(fmt, fid.read(4))[0] + 8

    str2 = fid.read(4)
    if str2 != b'WAVE':
        raise ValueError("Not a WAV file.")

    return file_size, is_big_endian



class WavFileWarning(UserWarning):
    pass

class wavinfo:

  def __init__(self, filename):

    if hasattr(filename, 'read'):
      fid = filename
      mmap = False
    else:
      fid = open(filename, 'rb')
      
    mmap = False
    try:

      file_size, is_big_endian = _read_riff_chunk(fid)
      fmt_chunk_received = False
      channels = 1
      bit_depth = 8
      format_tag = WAVE_FORMAT_PCM
      while fid.tell() < file_size:
        # read the next chunk
        chunk_id = fid.read(4)

        if not chunk_id:
          raise ValueError("Unexpected end of file.")
        elif len(chunk_id) < 4:
          raise ValueError("Incomplete wav chunk.")
        
        if chunk_id == b'fmt ':
          fmt_chunk_received = True
          self.fmt_chunk = _read_fmt_chunk(fid, is_big_endian)

          self.size, self.format_tag, self.channels, self.fs, self.bytes_per_second, self.block_align, self.bit_depth = self.fmt_chunk
        elif chunk_id == b'fact':
          _skip_unknown_chunk(fid, is_big_endian)
        elif chunk_id == b'data':
          if not fmt_chunk_received:
            raise ValueError("No fmt chunk before data")
          self.lenght = _read_size_data_chunk(fid, format_tag, channels, bit_depth,
                                              is_big_endian, mmap)
        elif chunk_id == b'LIST':    
          # Someday this could be handled properly but for now skip it
          _skip_unknown_chunk(fid, is_big_endian)
        elif chunk_id in (b'JUNK', b'Fake'):
          # Skip alignment chunks without warning
          _skip_unknown_chunk(fid, is_big_endian)
        else:
          warnings.warn("Chunk (non-data) not understood, skipping it.",
                        WavFileWarning)
          _skip_unknown_chunk(fid, is_big_endian)

    finally:
      if not hasattr(filename, 'read'):
        fid.close()
      else:
        fid.seek(0)


    # self.filename = filename
    # with io.open(self.filename, 'rb') as fh:
    #   riff, self.size1, fformat = struct.unpack('<4sI4s', fh.read(12))

    #   #Read header
    #   chunk_header = fh.read(8)
    #   # print(chunk_header)
    #   self.subID, self.chunkSize = struct.unpack('<4sI', chunk_header)
    #   # print(self.chunkSize)


    #   if (self.subID == b'fmt '):
    #     self.format, self.channels, self.samplerate, self.byterate, self.blockalign, self.bps = struct.unpack('HHIIHH', fh.read(16))

    #   chunkOffset = fh.tell()
    #   fh.seek(chunkOffset+self.chunkSize-16)
    #   # print(chunkOffset)
    #   self.chunkID, self.size = struct.unpack('<4sI', fh.read(8))
    #   # print(self.size/2)

    #   if self.format == 1:
    #     self.samples = int(self.size/2)
    #   else:
    #     self.samples = int(self.size*1.0)

    #   self.duration = self.samples / self.samplerate


  def getSizeB(self):
    # return self.size1
    return self.fmt_chunk

  def getchs(self):
    return self.channels

  def getSize(self):
    return int(self.lenght*8/self.bit_depth)

  def getRate(self):
    return self.fs

  def getDuration(self):
    return self.getSize()/self.fs



  # def read(self):
  #   with io.open(self.filename, 'rb') as fh:
  #     riff, size, fformat = struct.unpack('<4sI4s', fh.read(12))
  #     print("Riff: %s, Chunk Size: %i, format: %s" % (riff, size, fformat))

  #     #Read header
  #     chunk_header = fh.read(8)
  #     subchunkid, subchunksize = struct.unpack('<4sI', chunk_header)

  #     if (subchunkid == b'fmt '):
  #       aformat, channels, samplerate, byterate, blockalign, bps = struct.unpack('HHIIHH', fh.read(16))
  #       bitrate = (samplerate * channels * bps) / 1024
  #       print("Format: %i, Channels %i, Sample Rate: %i, Kbps: %i" % (aformat, channels, samplerate, bitrate))

  #     chunkOffset = fh.tell()
  #     while (chunkOffset < size):
  #       fh.seek(36)
  #       # print(chunkOffset)
  #       subchunk2id, subchunk2size = struct.unpack('<6sI', fh.read(10))
  #       print("chunk id: %s, size: %i" % (subchunk2id, subchunk2size))
  #       if (subchunk2id == b'LIST'):
  #         listtype = struct.unpack('<4s', fh.read(4))
  #         print("\tList Type: %s, List Size: %i" % (listtype, subchunk2size))
          
  #         listOffset = 0;
  #         while((subchunk2size - 8) >= listOffset):
  #           listitemid, listitemsize = struct.unpack('<4sI', fh.read(8))
  #           listOffset = listOffset + listitemsize + 8
  #           listdata = fh.read(listitemsize)
  #           print("\tList id %s, size: %i, data: %s" % (listitemid.decode('ascii'), listitemsize, listdata.decode("ascii")))
  #           print("\tOffset: %i" % listOffset)
  #       elif (subchunk2id == b'data'):
  #         print("Found data")
  #       else:
  #         print("Data: %s" % fh.read(subchunk2size).decode('ascii'))

  #       chunkOffset = chunkOffset + subchunk2size + 8

def _read_fmt_chunk(fid, is_big_endian):
    """
    Returns
    -------
    size : int
        size of format subchunk in bytes (minus 8 for "fmt " and itself)
    format_tag : int
        PCM, float, or compressed format
    channels : int
        number of channels
    fs : int
        sampling frequency in samples per second
    bytes_per_second : int
        overall byte rate for the file
    block_align : int
        bytes per sample, including all channels
    bit_depth : int
        bits per sample
    """
    if is_big_endian:
        fmt = '>'
    else:
        fmt = '<'

    size = res = struct.unpack(fmt+'I', fid.read(4))[0]
    bytes_read = 0

    if size < 16:
        raise ValueError("Binary structure of wave file is not compliant")

    res = struct.unpack(fmt+'HHIIHH', fid.read(16))
    bytes_read += 16

    format_tag, channels, fs, bytes_per_second, block_align, bit_depth = res

    if format_tag == WAVE_FORMAT_EXTENSIBLE and size >= (16+2):
        ext_chunk_size = struct.unpack(fmt+'H', fid.read(2))[0]
        bytes_read += 2
        if ext_chunk_size >= 22:
            extensible_chunk_data = fid.read(22)
            bytes_read += 22
            raw_guid = extensible_chunk_data[2+4:2+4+16]
            # GUID template {XXXXXXXX-0000-0010-8000-00AA00389B71} (RFC-2361)
            # MS GUID byte order: first three groups are native byte order,
            # rest is Big Endian
            if is_big_endian:
                tail = b'\x00\x00\x00\x10\x80\x00\x00\xAA\x00\x38\x9B\x71'
            else:
                tail = b'\x00\x00\x10\x00\x80\x00\x00\xAA\x00\x38\x9B\x71'
            if raw_guid.endswith(tail):
                format_tag = struct.unpack(fmt+'I', raw_guid[:4])[0]
        else:
            raise ValueError("Binary structure of wave file is not compliant")

    if format_tag not in KNOWN_WAVE_FORMATS:
        raise ValueError("Unknown wave file format")

    # move file pointer to next chunk
    if size > (bytes_read):
        fid.read(size - bytes_read)

    return (size, format_tag, channels, fs, bytes_per_second, block_align,
            bit_depth)


# assumes file pointer is immediately after the 'data' id
def _read_size_data_chunk(fid, format_tag, channels, bit_depth, is_big_endian,
                     mmap=False):
    if is_big_endian:
        fmt = '>I'
    else:
        fmt = '<I'

    # Size of the data subchunk in bytes
    size = struct.unpack(fmt, fid.read(4))[0]

    # # Number of bytes per sample
    # bytes_per_sample = bit_depth//8
    # if bit_depth == 8:
    #     dtype = 'u1'
    # else:
    #     if is_big_endian:
    #         dtype = '>'
    #     else:
    #         dtype = '<'
    #     if format_tag == WAVE_FORMAT_PCM:
    #         dtype += 'i%d' % bytes_per_sample
    #     else:
    #         dtype += 'f%d' % bytes_per_sample
    # if not mmap:
    #     data = numpy.frombuffer(fid.read(size), dtype=dtype)
    # else:
    #     start = fid.tell()
    #     data = numpy.memmap(fid, dtype=dtype, mode='c', offset=start,
    #                         shape=(size//bytes_per_sample,))
    #     fid.seek(start + size)

    # if channels > 1:
    #     data = data.reshape(-1, channels)
    return size


# import time
# from scipy.io.wavfile import read as wavread

# initial_time = time.time()

# wavstr = '/home/natan/Documents/TCC/testGitHubAccess/tcc-kw-spotting-fpga/PythonSpeechDataset/SpeechDatabase/soundNoise/noise-free-sound-0048.wav'
# wavstr = '/home/natan/Documents/TCC/testGitHubAccess/tcc-kw-spotting-fpga/PythonSpeechDataset/SpeechDatabase/newPortugueseVoicesDB/wav8k_files/Locutor3-KW.wav'
# wavFile = wavinfo(wavstr)
# chunk = wavFile.getSizeB()
# print(chunk)
# lenght = wavFile.getSize()
# print(lenght)
# # print('\n*******\n\nsize:', wavFile.getSize() )

# print(wavFile.getSize())
# print(wavFile.getSizeB())
# print(wavFile.getSize())
# print()
# wavFile.read()

# time1 = time.time()-initial_time
# print('\nduracao primeira leitura', time1)

# initial_time = time.time()
# print('\n*******\n\nsize:', len(wavread(wavstr)[1]))
# time2 = time.time()-initial_time
# print('\nduracao segunda leitura', time2)

# print(time2/time1)


# print(int('0'.join(c for c in '1a10s' if c.isdigit())))