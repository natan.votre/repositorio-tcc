import numpy as np
import inspect, re
import os
import sys
from librosa.core import resample
from utils.wavfile import read

# retorna apenas os numeros da string de entrada
def justnumbers(file):
      return ''.join(c for c in file if c.isdigit())


# return a statement with the probability given
def takeprob(prob):
	return np.random.rand(1) < prob

def randomUniform(N, amax=1, amin=0):
	return np.random.rand(N) * (amax-amin) + amin

def extract_frame(data, center, frame_len=512):
    
    # ajusta tipos
    center=int(center)
    frame_len = int(frame_len)
    frame_lenD2 = int(frame_len/2)

    N=len(data)

    # verifica possibilidades e completa com zeros
    if N < frame_len:
        pad = np.zeros(frame_len-N)
        frame = np.concatenate((pad, data))
    elif center<frame_lenD2:
        pad = np.zeros(frame_lenD2-center)
        frame = np.concatenate((pad, data[0:center+frame_lenD2]))
    elif center+frame_lenD2>N:
        pad = np.zeros(center+frame_lenD2-N)
        frame = np.concatenate((data[center-frame_lenD2:], pad))
    else:
        frame = data[center-frame_lenD2:center+frame_lenD2]
    
    if len(frame) != frame_len:
    	frame = frame[:frame_len]

    return frame

# imprime o nome da variavel
def varname(p):
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\bvarname\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if m:
            return m.group(1)

# print name of main variable
def whoisthis(func):
    this = sys.modules['__main__'].__dict__
    for key in this.keys():
        if id(func) == id(this[key]):
            return key
    
def giveNextName(name):

    aname = name
    i=0
    while(os.path.isfile(aname)):
        i+=1
        separate = os.path.splitext(name)
        aname = separate[0]+'_'+str(i)+separate[1]

    return aname

def splitData(x, y, p=0.2):
    
    # obtem tamanho de primeiro dataset
    len1 = int(x.shape[0]*(1-p))
    
    # configura primeiro dataset
    x1 = x[:len1]
    y1 = y[:len1]
    # configura segundo dataset a partir do tamanho do primeiro
    x2 = x[len1:]
    y2 = y[len1:]
        
    return (x1,y1), (x2,y2)

def splitArray(x, p=0.2):
    
    # obtem tamanho de primeiro dataset
    len1 = int(x.shape[0]*(1-p))
    
    # configura primeiro dataset
    x1 = x[:len1]
    
    # configura segundo dataset a partir do tamanho do primeiro
    x2 = x[len1:]
        
    return x1, x2

def modelNameByAcc(name, acc):

    basedir = os.path.dirname(name)

    str_acc= '{:.2f}'.format(acc*100)
    os.rename(name, os.path.join(basedir, 'model_'+str_acc+'p.hdf5'))
    print('file renamed by '+os.path.basename(name)+' to '+ 'model_'+str_acc+'p.hdf5')

# modelNameByAcc('oi/tudo/bem.s', 0)

def fill_buff(data, lenght, index=0):
    sh = data.shape
    x = np.zeros((sh[0], lenght))
    lenghtD2 = int(lenght/2)
    
    if sh[1] < lenght:
        x[:,:sh[1]] = data 
    elif index<lenght:
        x[:,lenght-index-1:] = data[:,:index+1]
    elif index>=sh[1] and index-sh[1]<lenght:
        x[:,:lenght-(index-sh[1]+1)] = data[:,index-lenght+1:]
    elif index-sh[1]<lenght:
        x = data[:,index-lenght+1:index+1]
    return x

# def fill_buff(data, lenght, index=0):
#     sh = data.shape
#     x = np.zeros((sh[0], lenght))
#     lenghtD2 = int(lenght/2)
    
#     if index<lenght:
#         x[:,lenght-index-1:] = data[:,:index+1]
#     elif index>=sh[1]:
#         x[:,:lenght-(index-sh[1]+1)] = data[:,index-lenght+1:]
#     else:
#         x = data[:,index-lenght+1:index+1]
#     return x


# def fill_buff(data, lenght, index=0):
#     sh = data.shape
#     x = np.zeros((sh[0], lenght))
    
#     for i in range(lenght):
#         if index-i>=0:
#             x[:,lenght-i-1] = data[:,index-i]
#         else:
#             break
#     return x
def logprox(x):

    return np.ceil(np.log2(x))

def wavread(file, fs):

    orig_fs, data = read(file)
    # if np.max(data) > 1:
    #     # print('oi')
    #     data = data/(2**logprox(np.max(data)))

    if fs == orig_fs:
        return data
    else:
        return resample(data, orig_fs, fs)


# import matplotlib.pyplot as plt
# data = wavread('/home/natan/Documents/TCC/testGitHubAccess/tcc-kw-spotting-fpga/PythonSpeechDataset/SpeechDatabase/newPortugueseVoicesDB/wav48k_files/Locutor1-KW.wav',
#                 8000)

# plt.plot(data)
# plt.show()
