fs = 8000                      # taxa de amostragem dos arquivos de audio

n_fft= 512                     # tamanho da FFT para extração dos MFCCs
n_fftp2 = int(n_fft/2)
hop_length=n_fftp2             # pulo entre cada frame
n_mels= 64                     # numero de filtros MEL
mfcc_number = 15               # numero de coeficientes MFFC
ofs_mfcc=2                     # offset dado para não utilizar os primeiros coeficientes MFCC      
n_mfcc= mfcc_number+ofs_mfcc   # numero de coeficientes MFFC


fmin=100    # frequencia mínima do MFCC
fmax=4000   # frequencia máxima do MFCC

n_frames_MFCC = 30 # numero de frames MFCC que será usado para o reconhecimento.

frame_len = int((n_frames_MFCC-1)*n_fft/2) # tamanho do frame recortado para cada entrada
frame_lenD2 = int(frame_len/2) # tamanho do frame dividido por 2


timeshift=1000
num_classes=3
nkw=3