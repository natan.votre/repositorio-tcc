import numpy as np

from keras.models import Sequential
from keras.layers import Activation, Conv2D, MaxPooling2D, Input, Dense, Flatten
from utils.layers import FlattenFPGA2D
from keras.layers import BatchNormalization, Dropout

def dims_of(sh):
	# sh = x.shape
	# print(sh)
	text = ''
	for i in range(len(sh)):
		text += str(sh[i])
		if i != len(sh)-1:
			text += 'x'

	return text

def plus_f(str_in, data):
    str_in[0] += data
    
def write_summary(filename, model, model_name):

  text = '/**** Summary of '+model_name+', where the Input Shape is: '+dims_of(model.input_shape[1:])+' \n\n'

  str_in = ['']
  model.summary(print_fn=lambda x: plus_f(str_in, x + '\n'))
  text += str_in[0]
  text += '\n\n****/\n\n'

  f = open(filename, 'w')
  f.write(text)
  f.close()


def createModel(input_shape=[], init_filters=4, final_filters=7, dense_len=50):

  num_classes = 4
  # define tipo de modelo Sequencial
  model = Sequential()
  print(input_shape)
  filters = np.round(np.linspace(init_filters, final_filters, 6)).astype(int)

  model.add(Conv2D(filters[0], kernel_size=(5, 5), strides=(1, 1),
                   input_shape=input_shape))
  model.add(BatchNormalization())
  model.add(Activation('relu'))

  model.add(Conv2D(filters[1], kernel_size=(4, 5), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(Conv2D(filters[2], kernel_size=(2, 5), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(Conv2D(filters[3], kernel_size=(2, 5), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(Conv2D(filters[4], kernel_size=(1, 4), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

  model.add(Conv2D(filters[5], kernel_size=(2, 3), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(FlattenFPGA2D())
  model.add(Dense(dense_len, activation='relu'))
  # numero de classes
  model.add(Dense(num_classes))
  model.add(Activation('softmax'))

  # compilacao da rede
  model.compile(loss='categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])

  return model


# toma o shape de entrada
input_shape = (15, 35, 1)

model1 = createModel(input_shape=input_shape, init_filters=4, final_filters=7,  dense_len=50)
model2 = createModel(input_shape=input_shape, init_filters=4, final_filters=7,  dense_len=70)
model3 = createModel(input_shape=input_shape, init_filters=4, final_filters=7,  dense_len=100)
model4 = createModel(input_shape=input_shape, init_filters=4, final_filters=10, dense_len=70)
model5 = createModel(input_shape=input_shape, init_filters=3, final_filters=10, dense_len=70)
model6 = createModel(input_shape=input_shape, init_filters=6, final_filters=10, dense_len=70)
model7 = createModel(input_shape=input_shape, init_filters=4, final_filters=12, dense_len=70)
model8 = createModel(input_shape=input_shape, init_filters=4, final_filters=14, dense_len=100)

write_summary('modelo1.txt', model1, 'modelo 1')
write_summary('modelo2.txt', model2, 'modelo 2')
write_summary('modelo3.txt', model3, 'modelo 3')
write_summary('modelo4.txt', model4, 'modelo 4')
write_summary('modelo5.txt', model5, 'modelo 5')
write_summary('modelo6.txt', model6, 'modelo 6')
write_summary('modelo7.txt', model7, 'modelo 7')
write_summary('modelo8.txt', model8, 'modelo 8')
