import numpy as np
import librosa

# %matplotlib widget
import matplotlib.pyplot as plt

from scipy.io.wavfile import write as wavwrite

from utils import utils
from utils.wav import wavinfo
from utils.envparams import *
   
import os
import time
 
import keras

def savePred(pred, path, file_df, suffix):
    
    # extrai informacoes do arquivo de audio
    wavFID = wavinfo(file_df)   # extrai cabeçalho do arquivo .wav
    smps = wavFID.getSize()
    
    
    # extende os pontos em quadrados para possuir o tamanho do arquivo original
    wave=np.zeros(smps)
    for i in range(len(pred)):
        limit = (i+1)*hop_length
        if limit > smps:
            wave[i*hop_length:]
            break
        else:
            wave[i*hop_length:limit] = pred[i]

    nameNoSuffix = os.path.splitext(os.path.basename(file_df))[0]

    if not os.path.isdir(path):
        os.makedirs(path)
    wavwrite(os.path.join(path, nameNoSuffix+suffix+'.wav'), fs, wave)          

def CreateDfMFCC(DictSet, root=''):
    # define a lista de mfccs
    DictSet['mfcc_file'] = []
    
    # varre os arquivos do dicionário
    for name in DictSet['file']:
        # obtém o arquivo de áudio
        data_file = utils.wavread(os.path.join(root,name), fs=fs)
        
        # extrai os atributos
        mfcc = librosa.feature.mfcc( y=data_file, 
                                     sr=fs, fmin=fmin, fmax=fmax, # **kwargs
                                     n_mfcc=n_mfcc, n_mels=n_mels, n_fft=n_fft,
                                     hop_length=hop_length)
        
        # anexa a lista
        DictSet['mfcc_file'].append(mfcc[ofs_mfcc:])

def CreateDfPntr(DictSet, idxs, root='', d1=0.33, d2=0.5, save=True):
    # define a lista de arquivo de ponteiros
    DictSet['pntr_file'] = []
    
    # restricoes
    if type(idxs)!= tuple:
        print('entrada idxs deve ser tupla')
        return
    
    if len(idxs) != num_classes-1:
        print('tamanho errado em idxs')
        return
    
    # varre os arquivos do dicionário
    for n in range(len(DictSet['file'])):
           
        # extrai informacoes do arquivo de audio
        # print(DictSet['file'][n])
        wavFID = wavinfo(os.path.join(root, DictSet['file'][n]))   # extrai cabeçalho do arquivo .wav
        # extrai numero de amostras
        smps = wavFID.getSize()
        
        # configura número de ponteiros
        # cada ponteiro corresponde a uma predicao
        pntrs_len = np.ceil(smps/hop_length).astype(int)        
        pntrs = np.zeros(pntrs_len)
        
        # varre os ponteiros que serao criados
        for i in range(pntrs_len):
            
            # varre os indexs do dicionario com o nome das keywords
            for j in range(len(idxs)):
                
                # varre as ocorrencias da keyword
                for k in range(len(DictSet[idxs[j]][n])):
                    
                    # verifica a existencia da keyword nas proximidades
                    y=0
                    if abs(DictSet[idxs[j]][n][k]/6 - i*hop_length) < d1*frame_len:
                        y=j+1
                        break
                    elif abs(DictSet[idxs[j]][n][k]/6 - i*hop_length) < d2*frame_len:
                        y=-1
                        break
                        
                # ocorrencia de uma keyword, sai do laço
                if y != 0:
                    break
                    
            # adquire o numero da keyword
            pntrs[i] = y
        
        # armazena o array de ponteiros
        DictSet['pntr_file'].append(pntrs)
        
        # salva como arquivo de áudio com tamanho do audio original
        if save==True:
            dirname = os.path.join(utils.whoisthis(DictSet), PATH_Pntrs)
            savePred(pntrs, os.path.join(PATH_resultsEval,dirname), os.path.join(root, DictSet['file'][n]), '_pntr')

def predClassDf(DictSet, model=[], root='', save=True):
    # define a lista de arquivo de ponteiros
    DictSet['pred_file'] = []
    
    # varre os arquivos do dicionário
    for n in range(len(DictSet['pntr_file'])):
        
        lendf=len(DictSet['pntr_file'][n])
        ydf=np.zeros(lendf)
        # varre cada ponteiro e realiza a predição de cada um
        for i in range(lendf):
            mfccsample = utils.fill_buff(DictSet['mfcc_file'][n], n_frames_MFCC, index=15+i).reshape(1,mfcc_number,n_frames_MFCC,1)
            
            ydf[i] = model.predict_classes(mfccsample)

        # if n>3:
        #     break

        # insere no dicionario
        DictSet['pred_file'].append(ydf)
        
        # salva como arquivo de áudio com tamanho do audio original
        if save==True:
            dirname = os.path.join(utils.whoisthis(DictSet), PATH_Pred)
            savePred(ydf, os.path.join(PATH_resultsEval,dirname), os.path.join(root,DictSet['file'][n]), '_pred')

def predClassDfBatch(DictSet, model=[], root='', save=True):
    # define a lista de arquivo de ponteiros
    DictSet['pred_file'] = []
    
    # varre os arquivos do dicionário
    for n in range(len(DictSet['pntr_file'])):
        
        lendf=len(DictSet['pntr_file'][n])
        ydf=np.zeros(lendf)
        
        mfccsamples = []
        # varre cada ponteiro e realiza a predição de cada um
        for i in range(lendf):
            mfccsamples.append(utils.fill_buff(DictSet['mfcc_file'][n], n_frames_MFCC, index=15+i).reshape(mfcc_number,n_frames_MFCC,1))
            
        mfccsamples = np.array(mfccsamples)
        ydf = model.predict_classes(mfccsamples, batch_size=512)

        # if n>3:
        #     break    
        
        # insere no dicionario
        DictSet['pred_file'].append(ydf)
        # DictSet['pred_file_batch'].append(ydf)
        
        # salva como arquivo de áudio com tamanho do audio original
        if save==True:
            dirname = os.path.join(utils.whoisthis(DictSet), PATH_Pred)
            savePred(ydf, os.path.join(PATH_resultsEval,dirname), os.path.join(root,DictSet['file'][n]), '_pred')

def SmoothPreds(DictSet, root='', d1=0.33, save=True):
    
    DictSet['pred_file_conv'] = []
    
    # varre cada arquivo de audio
    for j,_ in enumerate(DictSet['file']):
        
        list_conv = []
        # varre as kws
        for i in range(num_classes-1):

            # recupera o pred_df do dicionario
            pred_df = DictSet['pred_file'][j]*1.0

            # seleciona a classe da kw atual
            pred_df[pred_df != i+1] = 0
            pred_df[pred_df == i+1] = 1

            # define o tamanho da janela de acordo com a formulacao da parte positiva do pntr_df
            # dica: analisar sinal .wav gerado no CreateDfPntr
            wnd_len = np.round(frame_len*d1*2/hop_length).astype(int)
            # realiza suavização (smoothing)
            pred_df_smooth = np.convolve(pred_df, np.ones(wnd_len)/wnd_len, 'same')
            # lista as predicoes suavizadas
            list_conv.append(pred_df_smooth)
            
            # salva como arquivo de áudio com tamanho do audio original
            if save==True:
                dirname = os.path.join(utils.whoisthis(DictSet), PATH_PredSmooth)
                savePred(pred_df_smooth, os.path.join(PATH_resultsEval,dirname), 
                         os.path.join(root,DictSet['file'][j]),'_pred_smooth_kw'+str(i))
                
        # disposicao: DictSet['pred_file_conv']-[files]-[keywords]     
        DictSet['pred_file_conv'].append(list_conv)
    
def EvalKeywords(DictSet, idxs, thres, d1=0.33):

    # restricoes
    if type(idxs)!= tuple:
        print('entrada idxs deve ser tupla')
        return
    if type(thres)!= tuple:
        print('entrada thres deve ser tupla')
        return
    if len(idxs) != num_classes-1:
        print('tamanho errado em idxs')
        return
    if len(thres) != num_classes-1:
        print('tamanho errado em thres')
        return
        
    DictSet['ipreds'] = []
    # varre os tipos de kw
    for i in range(num_classes-1):
        # define novas listas
        
        list1 = []
        # varre os arquivos
        for j,_ in enumerate(DictSet[idxs[i]]):
            
            list2 = []
            # varre ocorrencias da kw
            for k,index in enumerate(DictSet[idxs[i]][j]):
                # indice para 8kHz
                index /= 6
                if index < 0:
                    list2.append(-1)
                    continue
                # estabelece inicio e fim do frame a partir do indice
                ini = np.ceil((index-frame_len*d1)/hop_length).astype(int)
                fim = np.ceil((index+frame_len*d1)/hop_length).astype(int)
                # retira o frame
                frame = DictSet['pred_file_conv'][j][i][ini:fim]
                # verifica se é superior ao threshold
                if np.max(frame) > thres[i]:
                    list2.append(1)
                else:
                    list2.append(0)
                    
            list1.append(np.array(list2))
        
        # disposicao: DictSet['ipreds']-[keywords]-[files]-[ikeyword_result]     
        DictSet['ipreds'].append(list1)

def EvalOOVs(DictSet, idxs, thres, root='', d1=0.33, save=True):
    
    # restricoes
    if type(idxs)!= tuple:
        print('entrada idxs deve ser tupla')
        return
    if type(thres)!= tuple:
        print('entrada thres deve ser tupla')
        return
    if len(idxs) != num_classes-1:
        print('tamanho errado em idxs')
        return
    if len(thres) != num_classes-1:
        print('tamanho errado em thres')
        return
        
        
    list1 = []
    # varre os arquivos
    for j,_ in enumerate(DictSet['file']):
        pred_oov=None
        # varre os tipos de kw
        for i in range(num_classes-1):
            # nao analisa arquivos de kw
            if max(DictSet[idxs[i]][j]) > 0:
                pred_oov=np.array(-1)
                list1.append(pred_oov)
                break
        # nao analisa arquivos de kw
        if pred_oov==-1:
            continue
            
        # define o tamanho da janela de acordo com a formulacao da parte positiva do pntr_df
        wnd_len = np.round(frame_len*d1*2/hop_length).astype(int)
        
        # predicao suavizada de todas kw
        pred_df = DictSet['pred_file_conv'][j]
        # cria array de tamanho pred_df
        pred_oov = np.array(pred_df[0])*0
        
        for k in range(len(pred_df[0])):
            # varre os tipos de kw
            for i in range(num_classes-1):
                # caso seja um, porque ja foi punido
                if pred_oov[k]!=0:
                    continue
                # se houve detecção, pune o array pred_oov                    
                if pred_df[i][k] > thres[i]:
                    pred_oov[k:k+wnd_len] = 1
                    continue
        
        list1.append(pred_oov)
        
        # salva como arquivo de áudio com tamanho do audio original
        if save:
            dirname = os.path.join(utils.whoisthis(DictSet), PATH_PredOOV)
            savePred(pred_oov, os.path.join(PATH_resultsEval,dirname), 
                     os.path.join(root,DictSet['file'][j]),'_pred_oov')
            
    DictSet['iOOVpreds'] = list1


def EvalPerformance(DictSet, idxs, thres, d1=0.33, save=False, verbose=0):
    
    DictAnalysis = {}
    
    # executa avaliadores de kw e de OOVs
    EvalKeywords(DictSet, idxs, thres, d1=0.33)   
    EvalOOVs(DictSet, idxs, thres, d1=0.33, save=save)
    
    # keywords evaluation
    for i in range(num_classes-1):
        # define dicionario temporario
        dictKW = {}
        
        total=0
        hits=0
        # varre os arquivos
        for j,_ in enumerate(DictSet['file']):
            # recupera as predicoes realizadas no EvalKeywords
            ipreds = DictSet['ipreds'][i][j]
            # acumula resultados
            total += len(ipreds[ipreds>=0])
            hits  += len(ipreds[ipreds==1])
        
        if total==0:
            total = 1
            
        # salva resultados
        dictKW['total']=total
        dictKW['hits'] =hits
        dictKW['acc']  =hits/total
        
        # armazena no dicionario final
        DictAnalysis['kw'+str(i)] = dictKW
        
    # OOVs evaluation
    dictOOVs={}
    total=0
    hits=0
    for j,ipredOOVs in enumerate(DictSet['iOOVpreds']):
        # acumula resultados
        total +=len(ipredOOVs[ipredOOVs>=0])
        hits  +=len(ipredOOVs[ipredOOVs==0])
        
    # salva resultados
    dictOOVs['total']=total
    dictOOVs['hits'] =hits
    dictOOVs['acc']  =hits/total
    dictOOVs['FArate']=(total-hits)/total

    # armazena no dicionario final
    DictAnalysis['OOV'] = dictOOVs
    
    DictAnalysis['verbose'] = ''
    if verbose==1:
        # imprime na tela
        print('*** Análise da performance do KWS ***\n\n')
        for i in range(num_classes-1):
            print('**Keyword '+str(i)+'**\n')
            print('- Total de ocorrencias:', DictAnalysis['kw'+str(i)]['total'])
            print('- Total de detecções:', DictAnalysis['kw'+str(i)]['hits'])
            print('- Acurácia: '+'{:.2f}'.format(DictAnalysis['kw'+str(i)]['acc']*100)+'%\n')

        print('**OOVs**\n')
        print('- Total de ocorrencias:', DictAnalysis['OOV']['total'])
        print('- Total de detecções:', DictAnalysis['OOV']['hits'])
        print('- Acurácia: '+'{:.2f}'.format(DictAnalysis['OOV']['acc']*100)+'%')
        print('- Taxa de Falsos Alarmes: '+'{:.2f}'.format(DictAnalysis['OOV']['FArate']*100)+'%\n')
    return DictAnalysis

def geomean(x):

    n = len(x)
    mult = 1
    for i in range(n):
        mult *= x[i]

    return mult**(1/n)

def arithmean(x):

    return np.mean(x)

def norm2(x):

    return np.linalg.norm(x, ord=2)

def calcBestThres(dictset, di=0, do=1, N=10, d1=0.33):

    steps = np.linspace(di, do, N)

    thres = [0,0,0]
    best  = {}
    best['thres'] = [0,0,0]
    best['score'] = 0
    best['analysis'] = []

    # brute force
    # nkw = 3
    for i in range(N):
        thres[0] = steps[i]
        for j in range(N):
            thres[1] = steps[j]
            for k in range(N):
                thres[2] = steps[k]

                time1 = time.time()
                anls = EvalPerformance(dictset, ('kw0','kw1','kw2'), tuple(thres), d1=d1, save=False, verbose=0)
                # score = norm2(np.array([anls['kw0']['acc'],anls['kw1']['acc'],anls['kw2']['acc'],anls['OOV']['acc']]))
                # score = arithmean(np.array([anls['kw0']['acc'],anls['kw1']['acc'],anls['kw2']['acc'],anls['OOV']['acc']]))
                score = geomean(np.array([anls['kw0']['acc'],anls['kw1']['acc'],anls['kw2']['acc'],anls['OOV']['acc']]))
                # print('Eval com Thres:',tuple(thres),'. Score:', score)

                if best['score'] < score:
                    best['thres'] = tuple(thres)
                    best['score'] = score
                    best['analysis'] = anls
                    print('Thresholds escolhidos:',best['thres'],'\nScore:', best['score'])

    return best                