import numpy as np
import matplotlib.pyplot as plt
import librosa

from scipy.io.wavfile import read as wavread
from scipy.io.wavfile import write as wavwrite

from utils import utils


from utils.wav import wavinfo
from utils.envparams import *

import os

def ExtractFeatureKW(dictSet, root='',
                          Amp_int=[0.5, 1.5], Amp_noise_int=[0.01, 0.1],
                          timeshift=500, Naugment=5, noise=None, 
                          proc=librosa.feature.mfcc,
                          namefile='nametosave', saveprob=0,
                          printby='label', print_ratio=10, path='none',
                          **kwargs):
    # imprime detalhes do processo
    
    # for name in utils.retrieve_name(dictSet):
    #     print(name)
    print('Processando dicionário principal - keywords.\n')

    # define limites do save prob
    if saveprob < 0 or saveprob > 1:
        saveprob = 0
    
    # é salvo algo ou nao
    if saveprob==0:
        savefile=False
    else:
        savefile=True
        
    N=Naugment
    
    # define o sinal de ruído zero se não foi enviado sinal.
    if type(noise)!=np.ndarray:
        noise=np.zeros(20000)
    
    allframes = {}
    for i in range(nkw):
        allframes['kw'+str(i)] = []
        dictSet['kw'+str(i)+'Feat'] = []
    
    num = 0    

    # varre todos os arquivos do dicionário
    for i in range(len(dictSet['file'])):
        
        if dictSet['type'][i] != 'KW':
            for kw in range(nkw):
                dictSet['kw'+str(kw)+'Feat'].append([])
            continue

        # lê o arquivo de áudio
        wavstr = os.path.join(root,dictSet['file'][i])
#         [_, data_file] = wavread(wavstr)
        data_file = utils.wavread(wavstr, fs)
        
        # normaliza amostras
        data_file = data_file/np.max(data_file)*0.5

        for kw in range(nkw):
            # inicializa listas utilizadas no proximo sweep
            framefile = []
            frameFeat = []
            
            # varre os ponteiros referentes aos arquivos de áudio
            for j in range(len(dictSet['kw'+str(kw)][i])):

                # se arquivo nao possuir ponteiro, nao continua
                if dictSet['kw'+str(kw)][i][j] < 0:
                    break
                    
                # configura ponteiro
                index = dictSet['kw'+str(kw)][i][j]/6

                # prepara o data augmented:
                # adquire valores aleatórios dentro dos intervalos de amplitude e deslocamento
                # RUIDO
                Anoise = utils.randomUniform(N, Amp_noise_int[1], Amp_noise_int[0])
                inoise = utils.randomUniform(N, len(noise)-frame_lenD2,frame_lenD2).astype(int)
                # AUDIO
                Aaudio = utils.randomUniform(N, Amp_int[1], Amp_int[0])
                ishift = utils.randomUniform(N, index+timeshift, index-timeshift).astype(int)


                # varre os N augmenting, realiza a extração de features
                # realiza o data augmentation
                for k in range(N):

                    # extrai frames ruido e audio com tamanho desejado
                    framenoise = Anoise[k]*utils.extract_frame(noise, inoise[k],
                                                frame_len=frame_len)
                    # ajusta deslocamento por alongamento realizado pelo time stretch
                    frameSample = Aaudio[k]*utils.extract_frame(data_file, ishift[k], 
                                                frame_len=frame_len)

                    # realiza a extração de features
                    FeatSample = proc(y=frameSample+framenoise, **kwargs)

                    # armazena na lista as features
                    frameFeat.append(FeatSample[ofs_mfcc:])

                    # salva frame a partir da probabilidade de salvar
                    if savefile:
                        if utils.takeprob(saveprob):
                            frameSample[0] = 1
                            allframes['kw'+str(kw)].append(frameSample+framenoise)
            # armazena no dicionario as features
            dictSet['kw'+str(kw)+'Feat'].append(frameFeat)


        # imprime na tela o andamento
        if printby=='number':
            num += 1
            if num == print_ratio:
                print('File processed:', wavstr)
                num = 0
        elif printby=='label':
            print('File processed:', wavstr)

    print('\n')
    
    # salva o arquivo de frames sortidos
    if savefile:
        for kw in range(nkw):
            frames = np.vstack(allframes['kw'+str(kw)]).reshape(-1)
            if not os.path.isdir(path):
                os.makedirs(path)
            wavwrite(os.path.join(path, namefile+'{0:.2f}'.format(saveprob)+'kw'+str(kw)+'.wav'), fs, frames)

def ExtractFeatureOOV(dictSet,proc=librosa.feature.mfcc,root='',
                          Amp_int=[0.5, 1.5], pntrRate=1, 
                          namefile='nametosave', saveprob=0,
                          printby='label', print_ratio=10, path='none',
                          **kwargs):
    # imprime detalhes do processo
    print('Processando dicionário principal - OOVs.\n')
    # print('Processando dicionário '+utils.retrieve_name(dictSet)+' - OOVs.\n')

    # define limites do save prob
    if saveprob < 0 or saveprob > 1:
        saveprob = 0
    
    # é salvo algo ou nao
    if saveprob==0:
        savefile=False
    else:
        savefile=True
    
        
    allframes = []
    dictSet['oovFeat'] = []
    
    num = 0    

    # varre todos os arquivos do dicionário
    for i in range(len(dictSet['file'])):
        
        if dictSet['type'][i] != 'OOV':
            dictSet['oovFeat'].append([])
            continue
        # lê o arquivo de áudio
        wavstr = os.path.join(root,dictSet['file'][i])
        data_file = utils.wavread(wavstr, fs)
        
        # normaliza amostras
        data_file = data_file/np.max(data_file)*0.5
        
        datalen = len(data_file)
        Npntrs  = int(np.round(datalen/frame_len*pntrRate))
        
        pntrs = utils.randomUniform(Npntrs, frame_lenD2, datalen-frame_lenD2).astype(int)
        
#         print(dictSet['file'][i])
#         print(datalen)
#         print(Npntrs)
#         print(pntrs)
#         break

        # inicializa listas utilizadas no proximo sweep
        framefile = []
        frameFeat = []
        
        # prepara o data augmented:
        # adquire valores aleatórios dentro dos intervalos de amplitude
        # AUDIO
        Aaudio = utils.randomUniform(Npntrs, Amp_int[1], Amp_int[0])
        

        # inicializa listas utilizadas no proximo sweep
        framelist = []
        Featlist = []

        # varre os ponteiros referentes aos arquivos de áudio
        for j in range(Npntrs):

            
            # ajusta deslocamento por alongamento realizado pelo time stretch
            frameSample = Aaudio[j]*utils.extract_frame(data_file, pntrs[j], 
                                        frame_len=frame_len)

            # realiza a extração de features
            FeatSample = proc(y=frameSample, **kwargs)

            # armazena no dicionario as features
            Featlist.append(FeatSample[ofs_mfcc:])

            # salva frame a partir da probabilidade de salvar
            if savefile:
                if utils.takeprob(saveprob):
                    frameSample[0] = 1
                    allframes.append(frameSample)
        # armazena no dicionario as features
        dictSet['oovFeat'].append(Featlist)


        # imprime na tela o andamento
        if printby=='number':
            num += 1
            if num == print_ratio:
                print('File processed:', wavstr)
                num = 0
        elif printby=='label':
            print('File processed:', wavstr)

    print('\n')
    
    # salva o arquivo de frames sortidos
    if savefile:
        frames = np.vstack(allframes).reshape(-1)
        if not os.path.isdir(path):
            os.makedirs(path)
        wavwrite(os.path.join(path, namefile+'{0:.2f}'.format(saveprob)+'oov.wav'), fs, frames)

def ExtractFeatureKWExpanse(dictSet, proc=librosa.feature.mfcc, 
                              Amp_int=[0.5, 1.5], Amp_noise_int=[0.01, 0.1],
                              timeshift=500, Naugment=5, noise=None, 
                              namefile='nametosave', saveprob=0,
                              printby='label', print_ratio=10, path='none', path_aug='path_aug',
                              **kwargs):

    # imprime detalhes do processo
    print('Processando dicionário principal exapndido - keywords .\n')
    # print('Processando dicionário '+utils.retrieve_name(dictSet)+' - KW DataAugmentation.\n')
#     print('Processando dicionário '+dictSet['name']+' - indice \''+idxSource+'\' externando na chave \''+typeExt+'\'.\n')
    
    # define limites do save prob
    if saveprob < 0 or saveprob > 1:
        saveprob = 0
    
    # é salvo algo ou nao
    if saveprob==0:
        savefile=False
    else:
        savefile=True
        
    N=Naugment
    
    # define o sinal de ruído zero se não foi enviado sinal.
    if type(noise)!=np.ndarray:
        noise=np.zeros(20000)
    
    allframes = {}
    num = 0
    totalfiles = sum([len(files) for r, d, files in os.walk(path_aug)])
    processedtotal = 0
    
    for kw in range(nkw):
        dictSet['kw'+str(kw)+'FeatAug'] = []
        allframes['kw'+str(kw)] = []
        
    # varre todos os arquivos do dicionário
    for i in range(len(dictSet['file'])):

        if dictSet['type'][i] != 'KW':
            for kw in range(nkw):
                dictSet['kw'+str(kw)+'FeatAug'].append([])
            continue

        wavstr = dictSet['file'][i]
        
        # extrai o nome base e remove sufixo '.wav'
        fnameNoSuffix = os.path.splitext(os.path.basename(wavstr))[0]
        
        # inicializa listas utilizadas no proximo sweep
        framelist = []
        Featlist = {}
        for kw in range(nkw):
            Featlist[kw] = []

        # caminha por todos os arquivos do DataAugmentation
        for pathx, dirs, files in os.walk(path_aug):
            
            # Apenas os diretorios utilizados no DictSet
            if pathx.find(fnameNoSuffix) != -1:

                # varre os arquivos expandidos
                for file in files:
                    
                    # lê o arquivo de áudio
                    wavstr = os.path.join(pathx, file)
                    data_file = utils.wavread(wavstr, fs=fs)
                    
                    # recupera os valores de pitch e stretch
                    [pitch, stretch] = list(map(float, 
                                            os.path.splitext(file)[0].split('--p')[1].split('s')))
                                        
                    for kw in range(nkw):
                        for j in range(len(dictSet['kw'+str(kw)][i])):
                            

                            # se arquivo nao possuir ponteiro, nao continua
                            if dictSet['kw'+str(kw)][i][j] < 0:
                                break
                            # configura ponteiro
                            index = dictSet['kw'+str(kw)][i][j]/6

                            # prepara o data augmented:
                            # adquire valores aleatórios dentro dos intervalos de amplitude e deslocamento
                            # RUIDO
                            Anoise = utils.randomUniform(N, Amp_noise_int[1], Amp_noise_int[0])
                            inoise = utils.randomUniform(N, len(noise)-frame_lenD2,frame_lenD2).astype(int)
                            # AUDIO
                            Aaudio = utils.randomUniform(N, Amp_int[1], Amp_int[0])
                            ishift = utils.randomUniform(N, index+timeshift, index-timeshift).astype(int)


                            # varre os N augmenting, realiza a extração de features
                            # realiza o data augmentation
                            for k in range(N):

                                # extrai frames ruido e audio com tamanho desejado
                                framenoise = Anoise[k]*utils.extract_frame(noise, inoise[k],
                                                            frame_len=frame_len)
                                # ajusta deslocamento por alongamento realizado pelo time stretch
                                frameSample = Aaudio[k]*utils.extract_frame(data_file, ishift[k]/stretch, 
                                                            frame_len=frame_len)

                                # realiza a extração de features
                                FeatSample = proc(y=frameSample+framenoise, **kwargs)

                                # armazena as features na lista
                                Featlist[kw].append(FeatSample[ofs_mfcc:])

                                # salva frame a partir da probabilidade de salvar
                                if savefile:
                                    if utils.takeprob(saveprob):
                                        frameSample[0] = 1
                                        allframes['kw'+str(kw)].append(frameSample+framenoise)

                    # imprime na tela o andamento
                    if printby=='number':
                        num += 1
                        processedtotal += 1
                        if num == print_ratio:
                            print(str(int(processedtotal/totalfiles*100))+'%, File processed:', wavstr)
                            num = 0
                    elif printby=='label':
                        print(str(int(processedtotal/totalfiles*100))+'%, File processed:', wavstr)
            
        for kw in range(nkw):
            # armazena as features na lista
            dictSet['kw'+str(kw)+'FeatAug'].append(Featlist[kw])

            
    print('100%\n')
    # salva o arquivo de frames sortidos
    if savefile:
        for kw in range(nkw):
            frames = np.vstack(allframes['kw'+str(kw)]).reshape(-1)
            if not os.path.isdir(path):
                os.makedirs(path)
            wavwrite(os.path.join(path, namefile+'{0:.2f}'.format(saveprob)+'kw'+str(kw)+'.wav'), fs, frames)

def ExtractFeatureOOVExpanse(dictSet, proc=librosa.feature.mfcc, 
                              Amp_int=[0.5, 1.5], pntrRate=1,  
                              namefile='nametosave', saveprob=0,
                              printby='label', print_ratio=10, path='none', path_aug='path_aug',
                              **kwargs):

    # imprime detalhes do processo
    print('Processando dicionário principal exapndido - OOVs .\n')
    # print('Processando dicionário '+utils.retrieve_name(dictSet)+' - KW DataAugmentation.\n')
#     print('Processando dicionário '+dictSet['name']+' - indice \''+idxSource+'\' externando na chave \''+typeExt+'\'.\n')
    
    # define limites do save prob
    if saveprob < 0 or saveprob > 1:
        saveprob = 0
    
    # é salvo algo ou nao
    if saveprob==0:
        savefile=False
    else:
        savefile=True
        
        

    num = 0
    totalfiles = sum([len(files) for r, d, files in os.walk(path_aug)])
    processedtotal = 0
    
    dictSet['oovFeatAug'] = []
    allframes = []
        
    # varre todos os arquivos do dicionário
    for i in range(len(dictSet['file'])):
        if dictSet['type'][i] != 'OOV':
            dictSet['oovFeatAug'].append([])
            continue

        datalen = dictSet['duration'][i]*fs
        Npntrs  = int(np.round(datalen/frame_len*pntrRate))
        # print(dictSet['duration'][i],'segundos')


        wavstr = dictSet['file'][i]
        
        # extrai o nome base e remove sufixo '.wav'
        fnameNoSuffix = os.path.splitext(os.path.basename(wavstr))[0]
        
        # inicializa listas utilizadas no proximo sweep
        framelist = []
        Featlist = []

        
        # caminha por todos os arquivos do DataAugmentation
        for pathx, dirs, files in os.walk(path_aug):
            
            # Apenas os diretorios utilizados no DictSet
            if pathx.find(fnameNoSuffix) != -1:

                # varre os arquivos expandidos
                for file in files:
                    
                    wavstr = os.path.join(pathx, file)
                    data_file = utils.wavread(wavstr, fs=fs)
                    
                    # recupera os valores de pitch e stretch
                    # print(wavstr)
                    [pitch, stretch] = list(map(float, 
                                            os.path.splitext(file)[0].split('--p')[1].split('s')))
                          
        
                    # prepara o data augmented:
                    # adquire valores aleatórios dentro dos intervalos de amplitude
                    # AUDIO
                    Aaudio = utils.randomUniform(Npntrs, Amp_int[1], Amp_int[0])
                    pntrs = utils.randomUniform(Npntrs, frame_lenD2, datalen-frame_lenD2).astype(int)
        


                    # varre os ponteiros referentes aos arquivos de áudio
                    for j in range(Npntrs):

                        
                        # ajusta deslocamento por alongamento realizado pelo time stretch
                        frameSample = Aaudio[j]*utils.extract_frame(data_file, pntrs[j]/stretch, 
                                                    frame_len=frame_len)

                        # realiza a extração de features
                        FeatSample = proc(y=frameSample, **kwargs)

                        # armazena no dicionario as features
                        Featlist.append(FeatSample[ofs_mfcc:])

                        # salva frame a partir da probabilidade de salvar
                        if savefile:
                            if utils.takeprob(saveprob):
                                frameSample[0] = 1
                                allframes.append(frameSample)

                    # imprime na tela o andamento
                    if printby=='number':
                        num += 1
                        processedtotal += 1
                        if num == print_ratio:
                            print(str(int(processedtotal/totalfiles*100))+'%, File processed:', wavstr)
                            num = 0
                    elif printby=='label':
                        print(str(int(processedtotal/totalfiles*100))+'%, File processed:', wavstr)
        # armazena no dicionario as features
        dictSet['oovFeatAug'].append(Featlist)

    print('100%\n')
    # salva o arquivo de frames sortidos
    if savefile:
        for kw in range(nkw):
            frames = np.vstack(allframes).reshape(-1)
            if not os.path.isdir(path):
                os.makedirs(path)
            namesave = os.path.join(path, namefile+'{0:.2f}'.format(saveprob)+'kw'+str(kw)+'.wav')
            wavwrite(namesave, fs, frames)
            print('Arquivo de frames salvo:', namesave)


def predictLenghtFeat(DictSet, Naugment=5, pntrRate=1):
    for name in utils.retrieve_name(DictSet):
        print(name)
    # print('***** Predição do conjunto '+utils.retrieve_name(DictSet)+' *****\n')
    Nkwx = {}
    for i in range(nkw):
        Nkwx[str(i)] = 0
    
        for j in range(len(DictSet['file'])):
            # print(DictSet['file'][j], DictSet['kw'+str(i)][j][0])
            if DictSet['kw'+str(i)][j][0] > 0:
                Nkwx[str(i)] += len(DictSet['kw'+str(i)][j])
    
    
        print('keyword '+str(i)+':', Nkwx[str(i)]*Naugment)
        
    total = 0
    for j in range(len(DictSet['file'])):
        if DictSet['type'][j] == 'OOV':
            datalen = np.round(DictSet['duration'][j]*fs)
            total += int(np.round(datalen/frame_len*pntrRate))
        
    print('OOV:', total)
    print('\n')

