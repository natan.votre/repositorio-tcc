########### MFCC Features #############

fs = 8000                      # taxa de amostragem dos arquivos de audio

n_fft= 512                     # tamanho da FFT para extração dos MFCCs
n_fftp2 = int(n_fft/2)
hop_length=n_fftp2             # pulo entre cada frame
n_mels= 64                     # numero de filtros MEL
mfcc_number = 15               # numero de coeficientes MFFC
ofs_mfcc=2                     # offset dado para não utilizar os primeiros coeficientes MFCC      
n_mfcc= mfcc_number+ofs_mfcc   # numero de coeficientes MFFC


fmin=100    # frequencia mínima do MFCC
fmax=4000   # frequencia máxima do MFCC

n_frames_MFCC = 35 # numero de frames MFCC que será usado para o reconhecimento.

frame_len = int((n_frames_MFCC-1)*n_fft/2) # tamanho do frame recortado para cada entrada
frame_lenD2 = int(frame_len/2) # tamanho do frame dividido por 2

############# Data Augmentation Params ############

kwaug_enable  = False
oovaug_enable = False
binsAug = [-1,1]
stretchAug = [0.9,1.1]
x_pitchAug = 2
x_stretchAug = 2

AugTotal = x_pitchAug*x_stretchAug

############ Extract Features Params #############

timeshift=1000
num_classes=4
nkw=3

ampl = [0.7, 1.5]
ampl_imnoise = [0.01, 0.2]

TSaugment = 30
PSaugment = int(TSaugment/(AugTotal*2))
pntrRate = 1.8
pntrRate_Aug = pntrRate/(AugTotal*2)

ampl_noise = [0.7, 1.5]
pntrRate_noise = 0.2


saveprobKW   = 0.01
saveprobKWaug= 0.01
saveprobOOVaug= 0.01
saveprobOOV  = 0.01
saveprobOOVn = 0.01


noise_invoice_enable = True

voiceoov_enable = True
voicekw_enable = True
noise_enable = True
PSkwaug_enable  = kwaug_enable
PSoovaug_enable = oovaug_enable

#########################

########### Create dataset ##########

# dataset division
testdiv  = 0.1
validdiv = 0.1
