import numpy as np
import matplotlib.pyplot as plt
import librosa

from scipy.io.wavfile import read as wavread
from scipy.io.wavfile import write as wavwrite

from utils import utils
from utils.wav import wavinfo
from envparams import *
from utils.extractfunctions import *

import sys
import os


def main(PATH_root, PATH_results):
  PATH_speechDB = os.path.join(PATH_root, 'SpeechDatabase')
  PATH_ptVoices = os.path.join(PATH_speechDB, 'newPortugueseVoicesDB')
  PATH_pt8k     = os.path.join(PATH_ptVoices, 'wav8k_files')
  PATH_pt48k     = os.path.join(PATH_ptVoices, 'wav48k_files')
  PATH_noise    = os.path.join(PATH_speechDB, 'soundNoise')


  PATH_resultsSpeechDB = os.path.join(PATH_results, 'SpeechDatabase')
  PATH_resultsDataaug  = os.path.join(PATH_ptVoices, 'DataAugmentation')
  PATH_resultsExtFeat  = os.path.join(PATH_results, 'ExtractFeatures')
  PATH_ExtFeatsamples  = os.path.join(PATH_resultsExtFeat, 'FrameSamples')
  PATH_ExtFeatdicts    = os.path.join(PATH_resultsExtFeat, 'Dicionarios') 

  mainPt  = np.load(os.path.join(PATH_resultsSpeechDB, 'mainPt.npy')).item()
  mainNoise = np.load(os.path.join(PATH_resultsSpeechDB, 'mainNoise.npy')).item()
  

  predictLenghtFeat(mainPt,    Naugment=TSaugment, pntrRate=pntrRate)
  predictLenghtFeat(mainNoise, Naugment=TSaugment, pntrRate=pntrRate_noise)


  if noise_invoice_enable:
    noise, _ = librosa.core.load(mainNoise['file'][0], sr=fs) # Lê todo o arquivo de audio
  else:
    noise = None

  if voicekw_enable:
    ExtractFeatureKW(mainPt, Naugment=TSaugment, proc=librosa.feature.mfcc, root=PATH_pt8k, 
                      Amp_int=ampl, Amp_noise_int=ampl_imnoise,
                      timeshift=timeshift, noise=noise,
                      namefile='mainPtSamples', saveprob=saveprobKW, path = PATH_ExtFeatsamples,
                      printby='number', print_ratio = 4,
                      sr=fs, fmin=fmin, fmax=fmax, # **kwargs
                      n_mfcc=n_mfcc, n_mels=n_mels, n_fft=n_fft,
                      hop_length=hop_length)

  if voiceoov_enable:
    ExtractFeatureOOV(mainPt, proc=librosa.feature.mfcc, root=PATH_pt8k,
                      Amp_int=ampl, pntrRate=pntrRate,
                      namefile='mainPtSamples', saveprob=saveprobOOV, path=PATH_ExtFeatsamples,
                      printby='number', print_ratio = 4,
                      sr=fs, fmin=fmin, fmax=fmax, # **kwargs
                      n_mfcc=n_mfcc, n_mels=n_mels, n_fft=n_fft, 
                      hop_length=hop_length)


  if noise_enable:
    ExtractFeatureOOV(mainNoise, proc=librosa.feature.mfcc, root='',
                      Amp_int=ampl_noise, pntrRate=pntrRate_noise,
                      namefile='trainNoise', saveprob=saveprobOOVn,
                      printby='number', print_ratio = 40, path=PATH_ExtFeatsamples,
                      sr=fs, fmin=fmin, fmax=fmax, # **kwargs
                      n_mfcc=n_mfcc, n_mels=n_mels, n_fft=n_fft, 
                      hop_length=hop_length)

  if PSkwaug_enable:
    ExtractFeatureKWExpanse(mainPt,       Naugment=PSaugment, proc=librosa.feature.mfcc,
                      Amp_int=ampl,       Amp_noise_int=ampl_imnoise, 
                      timeshift=timeshift,noise=noise,
                      namefile='trainKWAug',saveprob=saveprobKWaug,
                      printby='number',   print_ratio = 20,   path=PATH_ExtFeatsamples, path_aug=PATH_resultsDataaug,
                      sr=fs, fmin=fmin,   fmax=fmax, # **kwargs
                      n_mfcc=n_mfcc,      n_mels=n_mels,      n_fft=n_fft, 
                      hop_length=hop_length)

  if PSoovaug_enable:
    ExtractFeatureOOVExpanse(mainPt,      proc=librosa.feature.mfcc,
                      Amp_int=ampl,       pntrRate=pntrRate_Aug,
                      namefile='trainOOVAug', saveprob=saveprobOOVaug,
                      printby='number',   print_ratio = 20, path=PATH_ExtFeatsamples, path_aug=PATH_resultsDataaug,
                      sr=fs, fmin=fmin,   fmax=fmax, # **kwargs
                      n_mfcc=n_mfcc,      n_mels=n_mels, n_fft=n_fft, 
                      hop_length=hop_length)

  if not os.path.isdir(PATH_ExtFeatdicts):
    os.makedirs(PATH_ExtFeatdicts)

  np.save(os.path.join(PATH_ExtFeatdicts, 'mainPt.npy'), mainPt)
  print('escrito: '+'mainPt.npy')
  np.save(os.path.join(PATH_ExtFeatdicts, 'mainNoise.npy'),  mainNoise)
  print('escrito: '+'mainNoise.npy')



if __name__ == '__main__':
  # os.system('ls -l')
  print('\nExecutando o código', sys.argv[0],'\n\n')
  if len(sys.argv) > 1:
    PATH_root    = sys.argv[1] 
  else:
    PATH_root    = '../../'

  if len(sys.argv) > 2:
    PATH_results = sys.argv[2] 
  else:
    PATH_results = './results'
    
  main(PATH_root, PATH_results)
