import numpy as np
import matplotlib.pyplot as plt

from utils import utils
from envparams import *
from os import walk

import os
import sys
import time


def createArrayFeat(dictSet, keyFeat='kwFeat'):

    newlist = []
    for filelist in dictSet[keyFeat]:
      if filelist != []:
        for line in filelist:
          newlist.append(line)

    fileFeat = np.array(newlist)#.reshape(-1, 
                                          #          n_mfcc - ofs_mfcc, n_frames_MFCC)
    return fileFeat

def createblankDictType(refdict):

  thisdict = {}

  for key in refdict.keys():
    thisdict[key] = []
  
  return thisdict


def GetDataset(x):
    
    # aviso se entrada nao for tupla
    if type(x)!=tuple: 
        print('Entra deve ser Tupla')
        return
    
    # verifica número de classes que serao geradas 
    len_class = len(x)
    
    # define listas que serao utilizadas no "for"
    x_class = []
    y_class = []
    
    # varre as classes
    for i in range(len_class):
        
        # se possuir mais de um array na posicao da classe, concatena
        if type(x[i])==tuple: 
            x_class.append(np.concatenate(x[i]))
        else:
            x_class.append(x[i])
            
        # obtem o correspondente y para cada x
        y_class.append( (np.ones( (x_class[i].shape[0], 1) )*i).astype(int) )

    # gera x e y vazios
    xshape = list(x_class[0].shape)
    xshape[0] = 0
    x = np.zeros(tuple(xshape))
    yshape = list(y_class[0].shape)
    yshape[0] = 0
    y = np.zeros(tuple(yshape))
    
    # concatena os x e y
    for i in range(len_class):
        x = np.concatenate((x,x_class[i]))
        y = np.concatenate((y,y_class[i]))
    
    # toma o shape de x e y
    xshape = x.shape
    yshape = y.shape
    
    # concatena x e y e faz um scrambler (embaralhamento)
    x_n = x.reshape(xshape[0],-1)
    data = np.concatenate((x_n, y), axis=1)
    datascr = np.random.permutation(data)

    # gera mais uma dimensao em x para realizar o treinamento da rede (é necessario)
    xshape_new = list(xshape)
    xshape_new.append(1)
    
    # recupera x e y, agora embaralhados
    xscr = datascr[:,:datascr.shape[1]-yshape[1]].reshape(tuple(xshape_new))
    yscr = datascr[:,[datascr.shape[1]-yshape[1]]]

    return xscr, yscr

def main(PATH_root, PATH_results):

  PATH_speechDB = os.path.join(PATH_root, 'SpeechDatabase')
  PATH_ptVoices = os.path.join(PATH_speechDB, 'portugueseVoices')
  PATH_noise    = os.path.join(PATH_speechDB, 'soundNoise')

  PATH_resultsSpeechDB = os.path.join(PATH_results, 'SpeechDatabase')
  PATH_resultsDataaug  = os.path.join(PATH_results, 'DataAugmentation')
  PATH_resultsExtFeat  = os.path.join(PATH_results, 'ExtractFeatures')
  PATH_ExtFeatdicts    = os.path.join(PATH_resultsExtFeat, 'Dicionarios')
  
  PATH_dataset = os.path.join(PATH_results, '../Datasets/Dataset')


  mainPt  = np.load(os.path.join(PATH_ExtFeatdicts,   'mainPt.npy')).item()
  mainNoise = np.load(os.path.join(PATH_ExtFeatdicts, 'mainNoise.npy')).item()

  mainPt.keys()

  Nloc = max([int(utils.justnumbers(file)) for file in mainPt['file']])

  NlocTest =  int(Nloc*testdiv)
  NlocValid = int(Nloc*validdiv)
  NlocTrain = Nloc - NlocValid - NlocTest

  locShuffle = np.random.permutation(np.array([i+1 for i in range(Nloc)]))

  
  trainPt = createblankDictType(mainPt)
  testPt  = createblankDictType(mainPt)
  validPt = createblankDictType(mainPt)

  for key in mainPt.keys():
    print('key/len:', key, len(mainPt[key]))
  # try:
  for i in range(Nloc):
      for j in range(len(mainPt['file'])):
          if int(utils.justnumbers(mainPt['file'][j])) == locShuffle[i]:
              if i<NlocTrain:
                for key in mainPt.keys():
                  # print(i, j ,key)
                  trainPt[key].append(mainPt[key][j])
              elif i<NlocTrain+NlocTest:
                for key in mainPt.keys():
                  testPt[key].append(mainPt[key][j])
              else:
                for key in mainPt.keys():
                  validPt[key].append(mainPt[key][j])
  # except:
  #   print('i, j:', i, j)
  #   print('key:', key)

  print('**************** Sumário Locutores Features ****************\n\n')
  print('- Treinamento -\n')
  print('Número de Locutores:',NlocTrain)
  print('Locutores:',locShuffle[:NlocTrain])
  print('Número de arquivos Total:',len(trainPt['file']))

  print('\n')

  print('- Teste -\n')
  print('Número de Locutores:',NlocTest)
  print('Locutores:',locShuffle[NlocTrain:NlocTrain+NlocTest])
  print('Número de arquivos Total:',len(testPt['file']))

  print('\n')
  print('- Validação -\n')
  print('Número de Locutores:',NlocValid)
  print('Locutores:',locShuffle[NlocTrain+NlocTest:])
  print('Número de arquivos Total:',len(validPt['file']))


  if noise_enable:
    # analisa quantidade de arquivos no database de ruido
    Nnoise = 0
    for (dirpath, dirnames, filenames) in walk(PATH_noise):
        for file in filenames:
            if file.endswith('wav'):
                Nnoise += 1


    NnsTest =  int(Nnoise*testdiv)
    NnsValid = int(Nnoise*validdiv)
    NnsTrain = Nnoise - NnsValid - NnsTest

    # print(NnsTrain)
    # permuta arquivos para serem escolhidos aleatoriamente pelos conjuntos
    locShuffle = np.random.permutation(np.array([i for i in range(Nnoise)]))
    get_indexes = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]
    locShuffleC = locShuffle*0

    trainNoise  = createblankDictType(mainNoise)
    testNoise   = createblankDictType(mainNoise)
    validNoise  = createblankDictType(mainNoise)

    for i in range(Nnoise):
      j = get_indexes(i,locShuffle)[0]
      if i<NnsTrain:
        locShuffleC[j] = 1
      elif i<NnsTrain+NnsTest:
        locShuffleC[j] = 2
      else:
        locShuffleC[j] = 3

    i = 0
    for (dirpath, dirnames, filenames) in walk(PATH_noise):
      for file in filenames:
        if file.endswith('wav'):
            
          if locShuffleC[i] == 1:
            for key in mainNoise.keys():
              if type(mainNoise[key]) == list:
                  trainNoise[key].append(mainNoise[key][i])

          elif locShuffleC[i] == 2:
            for key in mainNoise.keys():
              if type(mainNoise[key]) == list:
                  testNoise[key].append(mainNoise[key][i])

          else:
            for key in mainNoise.keys():
              if type(mainNoise[key]) == list:
                  validNoise[key].append(mainNoise[key][i])

          i += 1

    times = 0
    for i in range(len(trainNoise['duration'])):
      times += trainNoise['duration'][i]
    trainNoise['allduration'] = times

    times = 0
    for i in range(len(testNoise['duration'])):
      times += testNoise['duration'][i]
    testNoise['allduration'] = times

    times = 0
    for i in range(len(validNoise['duration'])):
      times += validNoise['duration'][i]
    validNoise['allduration'] = times


    print('\n\n\n**************** Sumário Ruidos Features ****************\n\n')
    print('- Treinamento -\n')
    print('Número de arquivos:',NnsTrain)
    print('Duração total: '+time.strftime('%H:%M:%S', time.gmtime(trainNoise['allduration'])))


    print('\n')

    print('- Teste -\n')
    print('Número de arquivos:',NnsTest)
    print('Duração total: '+time.strftime('%H:%M:%S', time.gmtime(testNoise['allduration'])))

    print('\n')
    print('- Validação -\n')
    print('Número de arquivos:',NnsValid)
    print('Duração total: '+time.strftime('%H:%M:%S', time.gmtime(validNoise['allduration'])))





  train = {}
  test  = {}
  valid = {}

  if voicekw_enable:
    for kw in range(nkw):
      train['kw'+str(kw)] = createArrayFeat(trainPt, keyFeat='kw'+str(kw)+'Feat')
      test['kw'+str(kw)]  = createArrayFeat(testPt,  keyFeat='kw'+str(kw)+'Feat')
      valid['kw'+str(kw)] = createArrayFeat(validPt, keyFeat='kw'+str(kw)+'Feat')
  else:
    for kw in range(nkw):
      train['kw'+str(kw)] = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
      test['kw'+str(kw)]  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
      valid['kw'+str(kw)] = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)

  if voiceoov_enable:
    train['oov']  = createArrayFeat(trainPt, keyFeat='oovFeat')
    test['oov']   = createArrayFeat(testPt,  keyFeat='oovFeat')
    valid['oov']  = createArrayFeat(validPt, keyFeat='oovFeat')
  else:
    train['oov']  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
    test['oov']   = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
    valid['oov']  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)

  if noise_enable:
    train['oovN'] = createArrayFeat(trainNoise, keyFeat='oovFeat')
    test['oovN']  = createArrayFeat(testNoise, keyFeat='oovFeat')
    valid['oovN'] = createArrayFeat(validNoise, keyFeat='oovFeat')
  else:
    train['oovN']  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
    test['oovN']   = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
    valid['oovN']  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)

  if PSkwaug_enable:
    for kw in range(nkw):
      train['kw'+str(kw)+'A'] = createArrayFeat(trainPt, keyFeat='kw'+str(kw)+'FeatAug')
      test['kw'+str(kw)+'A']  = createArrayFeat(testPt,  keyFeat='kw'+str(kw)+'FeatAug')
      valid['kw'+str(kw)+'A'] = createArrayFeat(validPt, keyFeat='kw'+str(kw)+'FeatAug')
  else:
    for kw in range(nkw):
      train['kw'+str(kw)+'A'] = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
      test['kw'+str(kw)+'A']  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
      valid['kw'+str(kw)+'A'] = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)

  if PSoovaug_enable:
    train['oovA']  = createArrayFeat(trainPt, keyFeat='oovFeatAug')
    test['oovA']   = createArrayFeat(testPt,  keyFeat='oovFeatAug')
    valid['oovA']  = createArrayFeat(validPt, keyFeat='oovFeatAug')
  else:
    train['oovA']  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
    test['oovA']   = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)
    valid['oovA']  = np.array([]).reshape(-1,mfcc_number,n_frames_MFCC)


  x_train, y_train = GetDataset( ( (train['oov'], train['oovN'], train['oovA']),
                                   (train['kw0'], train['kw0A']), 
                                   (train['kw1'], train['kw1A']), 
                                   (train['kw2'], train['kw2A']) ) )

  x_test,  y_test  = GetDataset( ( (test['oov'], test['oovN'], test['oovA']),
                                   (test['kw0'], test['kw0A']), 
                                   (test['kw1'], test['kw1A']), 
                                   (test['kw2'], test['kw2A']) ) )

  x_valid, y_valid  = GetDataset( ( (valid['oov'], valid['oovN'], valid['oovA']),
                                    (valid['kw0'], valid['kw0A']), 
                                    (valid['kw1'], valid['kw1A']), 
                                    (valid['kw2'], valid['kw2A']) ) )


  print('\n\n************ Brief Summary of DataSet **************\n')

  print('-> Train dataset\n')

  print('All Classes size:', x_train.shape[0])
  print('OOV size:', x_train[y_train[:,0]==0].shape[0])
  for i in range(num_classes-1):
      print('Keyword '+str(i+1)+' size:', x_train[y_train[:,0]==i+1].shape[0])
  print('\nNoise OOV size:', train['oovN'].shape[0])
  print('Original speech OOV size:', train['oov'].shape[0])
  print('Augmented speech OOV size:', train['oovA'].shape[0])
  for i in range(num_classes-1):
      print('Original KW '+str(i+1)+' size:', train['kw'+str(i)].shape[0])
      print('Augmented KW '+str(i+1)+' size:', train['kw'+str(i)+'A'].shape[0])

  print('\n')
  print('-> Test dataset\n')

  print('All Classes size:', x_test.shape[0])
  print('OOV size:', x_test[y_test[:,0]==0].shape[0])
  for i in range(num_classes-1):
      print('Keyword '+str(i+1)+' size:', x_test[y_test[:,0]==i+1].shape[0])
  print('Original speech OOV size:', test['oov'].shape[0])
  print('\nNoise OOV size:', test['oovN'].shape[0])
  print('Original speech OOV size:', test['oov'].shape[0])
  print('Augmented speech OOV size:', test['oovA'].shape[0])
  for i in range(num_classes-1):
      print('Original KW '+str(i+1)+' size:', test['kw'+str(i)].shape[0])
      print('Augmented KW '+str(i+1)+' size:', test['kw'+str(i)+'A'].shape[0])

  print('\n')
  print('-> Validation dataset\n')

  print('All Classes size:', x_valid.shape[0])
  print('OOV size:', x_valid[y_valid[:,0]==0].shape[0])
  for i in range(num_classes-1):
      print('Keyword '+str(i+1)+' size:', x_valid[y_valid[:,0]==i+1].shape[0])
  print('Original speech OOV size:', valid['oov'].shape[0])
  print('\nNoise OOV size:', valid['oovN'].shape[0])
  print('Original speech OOV size:', valid['oov'].shape[0])
  print('Augmented speech OOV size:', valid['oovA'].shape[0])
  for i in range(num_classes-1):
      print('Original KW '+str(i+1)+' size:', valid['kw'+str(i)].shape[0])
      print('Augmented KW '+str(i+1)+' size:', valid['kw'+str(i)+'A'].shape[0])

  print('\n')
  # print(trainPt['kw1Feat'][4][0].shape)
  # for i in range(x_train.shape[0]):
  #   feat = trainPt['kw1Feat'][4][0]
  #   if (x_train[i,:,:,0]==feat).all():
  #     print('Está localizada na posição',i,'do dataset de treinamento')
  #     print('É da classe', y_train[i])
  #     plt.imshow(trainPt['kw1Feat'][4][0])
  #     plt.show()        
  #     plt.imshow(x_train[i,:,:,0])
  #     plt.show()

  sumf = 0
  for i in range(len(mainPt['file'])): 

    if mainPt['kw0'][i][0] > 0:
      sumf += len(mainPt['kw0Feat'][i])

  print('soma Aug:', sumf)

  print('x_train shape:', x_train.shape)


  
  pathsave = utils.giveNextFolder(PATH_dataset)

  if not os.path.isdir(pathsave):
      os.makedirs(pathsave)

  print('salvando caminho:', pathsave)
  np.save(os.path.join(pathsave, 'x_train.npy'), x_train)
  np.save(os.path.join(pathsave, 'y_train.npy'), y_train)
  print('Train "ok"')
  np.save(os.path.join(pathsave, 'x_test.npy'),  x_test)
  np.save(os.path.join(pathsave, 'y_test.npy'),  y_test)
  print('Test "ok"')
  np.save(os.path.join(pathsave, 'x_valid.npy'), x_valid)
  np.save(os.path.join(pathsave, 'y_valid.npy'), y_valid)
  print('Validation "ok"')

if __name__ == '__main__':
  # os.system('ls -l')
  print('\nExecutando o código', sys.argv[0],'\n\n')
  if len(sys.argv) > 1:
    PATH_root    = sys.argv[1] 
  else:
    PATH_root    = '../../'

  if len(sys.argv) > 2:
    PATH_results = sys.argv[2] 
  else:
    PATH_results = './results'
    
  main(PATH_root, PATH_results)
