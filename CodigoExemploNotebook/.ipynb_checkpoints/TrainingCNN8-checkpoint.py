import numpy as np
import matplotlib.pyplot as plt

from time import time

from utils import utils
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
import os
import sys

import keras
import keras.backend as K
from keras.models import Sequential
from keras.layers import Activation, Conv2D, MaxPooling2D, Input, Dense, Flatten
from utils.layers import FlattenFPGA2D
from keras.layers import BatchNormalization, Dropout

from keras.callbacks import TensorBoard
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 




def createModel6(input_shape=[], init_filters=4, final_filters=7, dense_len=50):

  num_classes = 4
  # define tipo de modelo Sequencial
  model = Sequential()
  print(input_shape)
  filters = np.round(np.linspace(init_filters, final_filters, 6)).astype(int)

  model.add(Conv2D(filters[0], kernel_size=(5, 5), strides=(1, 1),
                   input_shape=input_shape))
  model.add(BatchNormalization())
  model.add(Activation('relu'))

  model.add(Conv2D(filters[1], kernel_size=(4, 5), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(Conv2D(filters[2], kernel_size=(2, 5), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(Conv2D(filters[3], kernel_size=(2, 5), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(Conv2D(filters[4], kernel_size=(1, 4), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

  model.add(Conv2D(filters[5], kernel_size=(2, 3), strides=(1, 1)))
  model.add(Activation('relu'))

  model.add(FlattenFPGA2D())
  model.add(Dense(dense_len, activation='relu'))
  # numero de classes
  model.add(Dense(num_classes))
  model.add(Activation('softmax'))

  # compilacao da rede
  model.compile(loss='categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])

  return model



def main(PATH_dataset, modelname):
  num_classes = 4

  x_train = np.load(os.path.join(PATH_dataset, 'x_train.npy'))
  y_train = np.load(os.path.join(PATH_dataset, 'y_train.npy'))

  x_test = np.load(os.path.join(PATH_dataset, 'x_test.npy'))
  y_test = np.load(os.path.join(PATH_dataset, 'y_test.npy'))

  x_valid = np.load(os.path.join(PATH_dataset, 'x_valid.npy'))
  y_valid = np.load(os.path.join(PATH_dataset, 'y_valid.npy'))

  y_train_ce = keras.utils.to_categorical(y_train.reshape(-1), num_classes)
  y_test_ce = keras.utils.to_categorical(y_test.reshape(-1), num_classes)
  y_valid_ce = keras.utils.to_categorical(y_valid.reshape(-1), num_classes)


  # toma o shape de entrada
  input_shape = x_train.shape[1:]

  model = createModel6(input_shape=input_shape, init_filters=4, final_filters=14, dense_len=100)

  # print('\n\n************************ SUMÁRIO DA REDE ************************\n')
  # model.summary()

  # define o peso de cada classe (OOV sempre maior)
  class_weight = {0: 1,
                  1: 1,
                  2: 1.,
                  3: 1.}


  pathsave = os.path.join(os.path.join('trainresults',PATH_dataset), modelname)
  # pathsave = os.path.join(PATH_dataset, modelname)

  if not os.path.isdir(pathsave):
    os.makedirs(pathsave)


  # define o nome do arquivo contendo o modelo, não sobrescreve
  filepath= utils.giveNextName(os.path.join(pathsave, 'weights_best.hdf5'))

  # define o checkpoint para atualizar no arquivo o modelo
  checkpoint = keras.callbacks.ModelCheckpoint(filepath, monitor='val_acc', 
                                               verbose=0, save_best_only=True, 
                                               mode='max')

  tensorboard = TensorBoard(log_dir=os.path.join(pathsave,'logs/{}'.format(time())))

  # treina o modelo
  history = model.fit(x_train, y_train_ce,
            # epochs=5, 
            epochs=150, 
  #           validation_data=(x_test, y_test_ce),          
            validation_data=(x_valid, y_valid_ce),          
            class_weight=class_weight,
            callbacks=[checkpoint, tensorboard],
            batch_size=10100,
            verbose=0)
            # verbose=2)

  model = keras.models.load_model(filepath, custom_objects={'FlattenFPGA2D': FlattenFPGA2D})

  # realiza uma avaliação com o conjunto de teste
  score = model.evaluate(x_test, y_test_ce, batch_size=128)

  # altera o nome do arquivo pelo nome por model+acuracia+p.hdf5
  utils.modelNameByAcc(filepath,score[1])

  # imprime acuracia do conjunto de teste
  print('Acurácia do conjunto de teste: '+str(int(score[1]*100))+'%')

if __name__ == '__main__':
  # os.system('ls -l')
  print('\nExecutando o código', sys.argv[0],'\n\n')
  if len(sys.argv) > 1:
    PATH_dataset = sys.argv[1] 
  else:
    PATH_dataset = '../../' 

  if len(sys.argv) > 2:
    modelname = sys.argv[2] 
  else:
    modelname = 'modelname'
    
  main(PATH_dataset, modelname)
