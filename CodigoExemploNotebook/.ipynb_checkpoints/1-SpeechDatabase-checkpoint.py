import pandas as pd
import numpy as np

import os
from os import walk

from utils.wav import wavinfo
from utils import utils
from envparams import *

import sys

def format_index(p):

  p = p.replace('[','')
  p = p.replace(']','')
  p = p.replace(',','')

  p = p.split(' ')
  #     print(p)
  if p[0] != '':
    return list(map(int, p))
  else:
    return list(map(int,[-1e9])) 


def createDictSpeech():

  thisdict = {}

  thisdict['file'] = []
  thisdict['type']  = []
  thisdict['duration'] = []
  for i in range(nkw):
      thisdict['kw'+str(i)] = []
  
  return thisdict
  

def main(PATH_root, PATH_results):
  
  PATH_speechDB   = os.path.join(PATH_root,     'SpeechDatabase')
  PATH_ptVoices   = os.path.join(PATH_speechDB, 'newPortugueseVoicesDB')
  PATH_pt8k     = os.path.join(PATH_ptVoices,   'wav8k_files')
  PATH_ptVoices48 = os.path.join(PATH_ptVoices, 'wav48k_files')
  PATH_noise      = os.path.join(PATH_speechDB, 'soundNoise')
  
  PATH_resultsSpeechDB = os.path.join(PATH_results, 'SpeechDatabase')
  
  df = pd.read_csv(os.path.join(PATH_ptVoices48, 'helpfulTable.csv'))
  

  mainPt = createDictSpeech()


  Nloc = max([int(utils.justnumbers(file)) for file in df['filename']])
  for j in range(len(df)):
    mainPt['file'].append(df['filename'][j])
    mainPt['type'].append(df['type'][j])
    for k in range(nkw):
        mainPt['kw'+str(k)].append(format_index(df['kw'+str(k)][j]))
    mainPt['duration'].append(wavinfo(os.path.join(PATH_pt8k, df['filename'][j])).getDuration())


  
  print('\n**** Sumário Arquivos de Voz ****\n')
  print('Número de Locutores:',Nloc)
  print('Número de arquivos Total:',len(mainPt['file']))
  

  # analisa quantidade de arquivos no database de ruido
  Nnoise = 0
  for (dirpath, dirnames, filenames) in walk(PATH_noise):
      for file in filenames:
          if file.endswith('wav'):
              Nnoise += 1
  
   
  locShuffle = np.random.permutation(np.array([i for i in range(Nnoise)]))
  get_indexes = lambda x, xs: [i for (y, i) in zip(xs, range(len(xs))) if x == y]
  locShuffleC = locShuffle*0
  
  mainNoise = createDictSpeech()
  
  for (dirpath, dirnames, filenames) in walk(PATH_noise):
      for file in filenames:
          if file.endswith('wav'):
              
              # print(os.path.join(dirpath, file))
              mainNoise['file'].append(os.path.join(dirpath, file))
              for k in range(nkw):
                  mainNoise['kw'+str(k)].append([-1e9])
              mainNoise['duration'].append(wavinfo(os.path.join(dirpath, file)).getDuration())
              mainNoise['type'].append('OOV')

  time = 0
  for i in range(len(mainNoise['duration'])):
      time += mainNoise['duration'][i]
  mainNoise['allduration'] = time
  
  
  import time
  print('\n\n**** Sumário Arquivos de Ruído ****\n')
  print('Número de arquivos:',Nnoise)
  print('Duração total: '+time.strftime('%H:%M:%S', time.gmtime(mainNoise['allduration'])))
  
  
  if not os.path.isdir(PATH_resultsSpeechDB):
      os.makedirs(PATH_resultsSpeechDB)
  
  np.save(os.path.join(PATH_resultsSpeechDB, 'mainPt.npy'),  mainPt)
  np.save(os.path.join(PATH_resultsSpeechDB, 'mainNoise.npy'),  mainNoise)
    

if __name__ == '__main__':
  # os.system('ls -l')
  print('\nExecutando o código', sys.argv[0],'\n\n')
  if len(sys.argv) > 1:
    PATH_root    = sys.argv[1] 
  else:
    PATH_root    = '../../'

  if len(sys.argv) > 2:
    PATH_results = sys.argv[2] 
  else:
    PATH_results = './'
    
  main(PATH_root, PATH_results)
