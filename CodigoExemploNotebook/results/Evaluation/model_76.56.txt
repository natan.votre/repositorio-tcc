*** Análise da performance do KWS ***


Threholds escolhidos: (0.1, 0.1, 0.1)
Score: 0.7655653083140759

**Keyword 0**

- Total de ocorrencias: 355
- Total de detecções: 212
- Acurácia: 59.72%

**Keyword 1**

- Total de ocorrencias: 355
- Total de detecções: 252
- Acurácia: 70.99%

**Keyword 2**

- Total de ocorrencias: 355
- Total de detecções: 296
- Acurácia: 83.38%

**OOVs MainPt**

- Total de ocorrencias: 1252245
- Total de detecções: 1216957
- Acurácia: 97.18%
- Taxa de Falsos Alarmes: 2.82%

**OOVs MainNoise**
- Total de ocorrencias: 1401400
- Total de detecções: 1376827
- Acurácia: 98.25%
- Taxa de Falsos Alarmes: 1.75%

